//import java.util.HashMap;
//import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

public class Vehicles {

    private static Scanner scanner;
    final  static Logger logger = Logger.getLogger(String.valueOf(Vehicles.class));


    public static void menu() {
        scanner = new Scanner(System.in);
        String choose;
        boolean exit = false;

        do {

            logger.info("\nMOŻLIWE WYBORY:" + "\n1.SAMOLOT" + "\n2.SAMOCHÓD" + "\n3.ROWER" + "\n4.STATEK" + "\n5.EXIT");

            logger.info("WYBIERZ SPOŚRÓD PUNKTÓW (1-6): ");
            choose = scanner.nextLine();

            switch(choose) {

                case "1":

                    Plane.plane();
                    break;

                case "2":
                    Car.car();
                    break;

                case "3":
                    Bicycle.bicycle();
                    break;

                case "4":
                    Ship.ship();
                    break;

                case "5":
                    System.exit(0);
                    break;

                default:
                    logger.info("Zły wybór! Podaj prawidłową opcję!");


            }

        }while (!exit);

    }


//
//    public static void plane() {
//        String producer = "F16";
//        String producer2 = "TU154";
//        int speedMax = 680;
//        int speedMax2 = 740;
//        Map<String, Integer> Plane = new HashMap<String, Integer>();
//        Plane.put(producer, speedMax);
//        Plane.put(producer2, speedMax2);
//        logger.info("Moje samoloty: " + Plane);
//        if(speedMax > speedMax2) {
//            logger.info("Samolot producenta" + producer + " jest najszybszy, jego maksymalna prędkość to: " +speedMax + " km/h");
//        }
//        else if(speedMax < speedMax2)
//        {
//            logger.info("Samolot producenta " + producer2 +" jest najszybszy, jego maksymalna prędkość to: " +speedMax2 + " km/h");
//        }
//        else if (speedMax == speedMax2)
//        {
//            logger.info(producer + " i " + producer2 + " poruszają się z taką samą prędkością");
//        }
//
//
//    }
//
//    public static void car() {
//        String producer = "Mercedes";
//        String producer2 = "Volksvagen ";
//        int speedMax = 220;
//        int speedMax2 = 275;
//        Map<String, Integer> Car = new HashMap<String, Integer>();
//        Car.put(producer, speedMax);
//        Car.put(producer2, speedMax2);
//        logger.info("Moje auta: " + Car);
//        if(speedMax > speedMax2) {
//            logger.info("Samochód producenta" + producer + "jest najszybszy, jego maksymalna prędkość to: " +speedMax + " km/h");
//        }
//        else if(speedMax < speedMax2)
//        {
//            logger.info("Samochód producenta" +producer2 + "Audi jest najszybszy, jego maksymalna prędkość to: " +speedMax2 + " km/h");
//        }
//        else if (speedMax == speedMax2)
//        {
//            logger.info(producer + " i " + producer2 + " poruszają się z taką samą prędkością");
//        }
//
//    }
//
//
//    public static void bicycle() {
//        String producer = "Grand";
//        String producer2 = "RockRider";
//        int speedMax = 130;
//        int speedMax2 = 140;
//        Map<String, Integer> Bicycle = new HashMap<String, Integer>();
//        Bicycle.put(producer, speedMax);
//        Bicycle.put(producer2, speedMax2);
//        logger.info("Moje rowery: " + Bicycle);
//        if(speedMax > speedMax2) {
//            logger.info("Rower producenta" + producer +  "jest najszybszy, jego maksymalna prędkość to: " +speedMax + " km/h");
//        }
//        else if(speedMax < speedMax2)
//        {
//            logger.info("Rower producenta" + producer2 +  " jest najszybszy, jego maksymalna prędkość to: " +speedMax2 + " km/h");
//        }
//        else if (speedMax == speedMax2)
//        {
//            logger.info(producer + " i " + producer2 + " poruszają się z taką samą prędkością");
//        }
//
//
//    }
//    public static void ship() {
//        String producer = "Tytanic";
//        int speedMax = 23;
//        String producer2 = "Danuta";
//        int speedMax2 = 40;
//        Map<String, Integer> Ship = new HashMap<String, Integer>();
//        Ship.put(producer, speedMax);
//        Ship.put(producer2, speedMax2);
//        logger.info("Moje statki: " + Ship);
//        if(speedMax > speedMax2) {
//            logger.info("Statek producenta " +producer + " jest najszybszy, jego maksymalna prędkość to: " +speedMax + " km/h");
//        }
//        else if(speedMax < speedMax2)
//        {
//            logger.info("Statek producenta " + producer2 + " jest najszybszy, jego maksymalna prędkość to: " +speedMax2 + " km/h");
//        }
//        else if (speedMax == speedMax2)
//        {
//            logger.info(producer + " i " + producer2 + " poruszają się z taką samą prędkością");
//        }
//
//
//    }


}